add_executable(planar_robot ${CMAKE_CURRENT_SOURCE_DIR}/planar_robot/main.cpp)
target_link_libraries(planar_robot PUBLIC robot_model)

add_executable(nao_arms ${CMAKE_CURRENT_SOURCE_DIR}/nao_arms/main.cpp)
target_link_libraries(nao_arms PUBLIC robot_model)