# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.16

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/matthieu/robot-model

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/matthieu/robot-model

# Include any dependencies generated for this target.
include apps/CMakeFiles/planar_robot.dir/depend.make

# Include the progress variables for this target.
include apps/CMakeFiles/planar_robot.dir/progress.make

# Include the compile flags for this target's objects.
include apps/CMakeFiles/planar_robot.dir/flags.make

apps/CMakeFiles/planar_robot.dir/planar_robot/main.cpp.o: apps/CMakeFiles/planar_robot.dir/flags.make
apps/CMakeFiles/planar_robot.dir/planar_robot/main.cpp.o: apps/planar_robot/main.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/matthieu/robot-model/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object apps/CMakeFiles/planar_robot.dir/planar_robot/main.cpp.o"
	cd /home/matthieu/robot-model/apps && /usr/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/planar_robot.dir/planar_robot/main.cpp.o -c /home/matthieu/robot-model/apps/planar_robot/main.cpp

apps/CMakeFiles/planar_robot.dir/planar_robot/main.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/planar_robot.dir/planar_robot/main.cpp.i"
	cd /home/matthieu/robot-model/apps && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/matthieu/robot-model/apps/planar_robot/main.cpp > CMakeFiles/planar_robot.dir/planar_robot/main.cpp.i

apps/CMakeFiles/planar_robot.dir/planar_robot/main.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/planar_robot.dir/planar_robot/main.cpp.s"
	cd /home/matthieu/robot-model/apps && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/matthieu/robot-model/apps/planar_robot/main.cpp -o CMakeFiles/planar_robot.dir/planar_robot/main.cpp.s

# Object files for target planar_robot
planar_robot_OBJECTS = \
"CMakeFiles/planar_robot.dir/planar_robot/main.cpp.o"

# External object files for target planar_robot
planar_robot_EXTERNAL_OBJECTS =

bin/planar_robot: apps/CMakeFiles/planar_robot.dir/planar_robot/main.cpp.o
bin/planar_robot: apps/CMakeFiles/planar_robot.dir/build.make
bin/planar_robot: lib/librobot_model.a
bin/planar_robot: /home/matthieu/.conan/data/fmt/7.0.1/_/_/package/b911f48570f9bb2902d9e83b2b9ebf9d376c8c56/lib/libfmt.a
bin/planar_robot: /home/matthieu/.conan/data/urdfdomcpp/1.0/bnavarro/stable/package/54f23fb18bed2d62bb8dc8b006686d8e7278d11c/lib/liburdfdomcpp.a
bin/planar_robot: /home/matthieu/.conan/data/tinyxml2/8.0.0/_/_/package/b911f48570f9bb2902d9e83b2b9ebf9d376c8c56/lib/libtinyxml2.a
bin/planar_robot: apps/CMakeFiles/planar_robot.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/home/matthieu/robot-model/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking CXX executable ../bin/planar_robot"
	cd /home/matthieu/robot-model/apps && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/planar_robot.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
apps/CMakeFiles/planar_robot.dir/build: bin/planar_robot

.PHONY : apps/CMakeFiles/planar_robot.dir/build

apps/CMakeFiles/planar_robot.dir/clean:
	cd /home/matthieu/robot-model/apps && $(CMAKE_COMMAND) -P CMakeFiles/planar_robot.dir/cmake_clean.cmake
.PHONY : apps/CMakeFiles/planar_robot.dir/clean

apps/CMakeFiles/planar_robot.dir/depend:
	cd /home/matthieu/robot-model && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/matthieu/robot-model /home/matthieu/robot-model/apps /home/matthieu/robot-model /home/matthieu/robot-model/apps /home/matthieu/robot-model/apps/CMakeFiles/planar_robot.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : apps/CMakeFiles/planar_robot.dir/depend

