#include <fmt/core.h>
#include <umrob/robot_model.h>

#include <urdf_model/pose.h>
#include <urdf_parser/urdf_parser.h>
#include <fmt/format.h>
#include <fmt/ostream.h>

#include <stdexcept>
#include <cstdio>
#include <cstring>
#include <iostream>
namespace umrob {

    RobotModel::RobotModel([[maybe_unused]] const std::string& urdf) {

    std::cout << "\n\n--- Loading URDF C++ model ---\n\n";
    model_ = urdf::parseURDF(urdf); // model_ is now made of 
    std::cout << "\n\n--- URDF C++ model loaded ---\n\n";

    urdf::LinkConstSharedPtr link;    // creating link pointer
    urdf::JointConstSharedPtr joint;    // creating joint pointer

    std::cout << "\n\n==========================================\n==========================================\n===    Showing URDF Parser features   ====\n==========================================\n==========================================\n\n";

    // Get root link 
    link = model_->getRoot();    // "link" is "linked" to the root link of the model "model_"

    // Link name
    std::cout << "  Name of the root link: " << link->name << "\n\n"; // displaying the link name 
 

    // "link->child_joints" return a std::vector<JointConstSharedPtr> of every child joint of a link
    joint = link->child_joints[0];
    std::cout << "  Name of its first child joint:" <<  joint->name << "\n\n";

    // "joint->type" returns the type "urdf::Joint"  
    /// \brief     type_       meaning of axis_
    /// ------------------------------------------------------
    ///            UNKNOWN     unknown type
    ///            REVOLUTE    rotation axis
    ///            PRISMATIC   translation axis
    ///            FLOATING    N/A
    ///            PLANAR      plane normal axis
    ///            FIXED       N/A
    std::cout << "  Is this a CONTINUOUS joint (?): " << (joint->type == urdf::Joint::CONTINUOUS) << "\n\n";
    std::cout <<  "  Is this a PLANAR joint (?): " <<(joint->type == urdf::Joint::PLANAR) << "\n\n";

    // The std::map members related to the joints may be initialized, since we know each joint->name
    joints_position_[joint->name] = 0;
    // Same for joint limits, which belongs to type JointLimits
    JointLimits limits;
    if (joint->limits) {
        limits.min_position = joint->limits->lower;
        limits.max_position = joint->limits->upper;
        limits.max_velocity = joint->limits->velocity;
        limits.max_effort = joint->limits->effort;
    }
    joints_limits_[joint->name] = limits;



    // A joint has one (and only one) child link. It name can be accessed with joint->child_link_name
    // Method getlink(name) of a urdf model returns the link with name given by a string
    std::cout <<  "  The name of its child link is " << joint->child_link_name  << "\n\n";
    link = model_->getLink(joint->child_link_name);

    // The axis of a joint is obtained with joint->axis. Terms x, y and z, are given seperately
    std::cout << "  Joint axis: [" << joint->axis.x << ", " << joint->axis.y << ", " << joint->axis.z << "]\n\n";


    // The rigid body transformation of a joint from its parent joint is obtained with joint->parent_to_joint_origin_transform;
    // It returns an object of class Pose, which has, among others, the following members:
        // Vector3  position;
        // Rotation rotation;
    urdf::Pose origin = joint->parent_to_joint_origin_transform;
    // Class Vector3 has notably the members x, y and z
    // Class Rotation has notably the member function 
    //      void getQuaternion(double &quat_x,double &quat_y,double &quat_z, double &quat_w) const
    double qx, qy, qz, qw;
    origin.rotation.getQuaternion(qx,qy,qz,qw);

    
    // Eigen::AngleAxisd may be particularly usefull to generate a roration matrix based on an angle and an axis:
    Eigen::AngleAxisd(M_PI,Eigen::Vector3d(0,0,1) ).toRotationMatrix();

}



double& RobotModel::jointPosition(const std::string& name) {
    return joints_position_.at(name);
}

size_t RobotModel::jointIndex(const std::string& name) const {
        int joint_index;
        joint_index=0;
        for (auto k=joints_position_.begin();k!=joints_position_.end();k++){
        if (k->first==name)
        {
            return joint_index;
        }
        else
        {
            joint_index=joint_index+1;
        }
        }

        return joint_index; // give higher index if not in the map
        
}
    

const std::string& RobotModel::jointNameByIndex(size_t index) const {
        size_t joint_index=0; // must be of type size_t to be able to compare with input index
        for (auto k=joints_position_.begin();k!=joints_position_.end();k++){
        if (joint_index==index)
        {
            return k->first;
        }
        else
        {
            joint_index=joint_index+1; // if it is not the right index, let's compare with the next one
        }
        }
        return joints_position_.end()->first;
}

void RobotModel::update() {
    // TODO: implement // LOOKS HARD, MAYBE LATER
}

const Eigen::Affine3d&
RobotModel::linkPose(const std::string& link_name) const {
    return links_pose_.at(link_name);
}

const Jacobian& RobotModel::linkJacobian(const std::string& link_name) {
        for (auto k=links_jacobian_.begin();k!=links_jacobian_.end();k++)
        {
            if (k->first==link_name)
            {
                return k->second;
            }
        }
        return links_jacobian_.end()->second;
}

const JointLimits& RobotModel::jointLimits(const std::string& joint_name) const {
    
    return joints_limits_.at(joint_name);
}

size_t RobotModel::degreesOfFreedom() const {
    // TODO: implement // LOOKS HARD, MAYBE LATER
    return 0;
}

std::vector<urdf::JointConstSharedPtr> RobotModel::jacobianJointChain([
    [maybe_unused]] const std::string& link_name) const {
    std::vector<urdf::JointConstSharedPtr> joint_chain;
    for(auto k=joints_position_.begin();k!=joints_position_.end();k++)
    {
        //joint_chain.push_back(*k)
        // TO DO
    }
    
    //joint_chain.push_back(joints_position_[])
    return joint_chain;
}


void RobotModel::addInfoLink(urdf::LinkConstSharedPtr link_ptr, urdf::ModelInterfaceSharedPtr model_){
    
    // LINK INFO ADDING NOT IMPLEMENTED FOR LINKS_POSE AND LINKS_JACOBIAN

    // RECURSIVE FUNCTION TO ALL CHILD JOINTS
    joints_vect=link_ptr->child_joints;
    for(auto k=0;k!=size(joints_vect);k++)
    {
        auto joint_ptr=link_ptr->child_joints[k];
        RobotModel::addInfoJoint(joint_ptr,model_); // CALL THE FUNCTION FOR EACH CHILD JOINTS

    }
}

void RobotModel::addInfoJoint(urdf::JointConstSharedPtr joint_ptr, urdf::ModelInterfaceSharedPtr model_){

    // JOINT LIMIT MAP FILLING
    RobotModel::JointLimits limits;
    if (joint->limits) {
        limits.min_position = joint->limits->lower;
        limits.max_position = joint->limits->upper;
        limits.max_velocity = joint->limits->velocity;
        limits.max_effort = joint->limits->effort;
    }
    joints_limits_[joint_ptr->name] = limits;

    // RECURSIVE FUNCTION, A JOINT OBVIOUSLY HAVE AN UNIQUE CHILD LINK
    link_ptr=model_->getLink(joint_ptr->child_link_name);
    RobotModel::addInfoLink(link_ptr,model_);
}

}   